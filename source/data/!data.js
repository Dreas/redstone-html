var data = {
    topMenu: [
        {
            link: '#work',
            name: 'work',
        },
        {
            link: '#shop',
            name: 'shop',
        },
        {
            link: '#insta',
            name: 'insta',
        },
        {
            link: '#about',
            name: 'about',
        },
    ],
    items: [
        {
            img: '003.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '004.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '005.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '006.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '007.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '008.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '009.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '011.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '014.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '015-1.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '015-2.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '018.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '025.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '026.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '027-1.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '028-1.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '029-2.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '030-2.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '031.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
        {
            img: '034-1.jpg',
            title: 'Рождение',
            subtitle: 'Холст масло',
            color: '#ffffff'
        },
    ],
};

module.exports = data;