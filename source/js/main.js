$(document).ready(function () {
    $('.b-box__i').masonry({
        itemSelector: '.b-box__elem',
        columnWidth: '.b-box__elem_x',
        percentPosition: true,
    });
});